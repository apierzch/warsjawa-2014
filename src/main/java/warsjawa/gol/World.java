package warsjawa.gol;

public class World {

    private Integer[][] board;

    public World(Integer[][] board) {
        this.board = board;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Integer[] row : board) {
            for (int cell : row) {
                sb.append(cell).append(",");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public void nextGen() {

    }
}
