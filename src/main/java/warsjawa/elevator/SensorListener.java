package warsjawa.elevator;

public interface SensorListener{

    public void floorReached(int floorNumber);
}

