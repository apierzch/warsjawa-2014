package warsjawa.gol;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class WorldTest {

    @Test
    public void singleDies() {
        World world = new World($(
                $(0, 0, 0, 0),
                $(0, 1, 0, 0),
                $(0, 0, 0, 0),
                $(0, 0, 0, 0)
        ));

        world.nextGen();

        assertThat(world).isEqualTo(new World($(
                $(0, 0, 0, 0),
                $(0, 0, 0, 0),
                $(0, 0, 0, 0),
                $(0, 0, 0, 0)
        )));
    }

    @SafeVarargs
    public static <T> T[] $(T... array) {
        return array;
    }
}